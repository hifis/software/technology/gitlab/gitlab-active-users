# GitLab: Number of Active Users

## Installation

The project can be installed via Poetry.
It depends on

* [click](https://github.com/pallets/click/yth)
* [python-gitlab](https://github.com/python-gitlab/python-gitlab)

```bash
poetry install
```

## Usage

To execute the script run this command:

```bash
poetry run python active_users.py active-users --host gitlab.hzdr.de
```

The script will prompt for an API read token with admin permissions.
