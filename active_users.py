import datetime

import click
import gitlab

ACTIVE_DAYS = 90


@click.group()
def cli():
    """CLI command group."""
    pass


@cli.command()
@click.option(
    "--token", prompt=True, hide_input=True, confirmation_prompt=True, type=str
)
@click.option("--host", type=str, default="gitlab.hzdr.de")
def active_users(token, host):
    """Get number of active users from the GitLab API."""
    active_users = 0
    active_days_delta = datetime.timedelta(days=ACTIVE_DAYS)
    reference_date = datetime.date.today()

    gl = gl_api(host, token)
    users = gl.users.list(as_list=False)
    total_users = users.total

    click.echo(f"Total number of users: { total_users }")

    for user in users:
        if not user.attributes["last_activity_on"]:
            continue
        last_activity_date = datetime.date.fromisoformat(
            user.attributes["last_activity_on"]
        )
        last_active_delta = reference_date - last_activity_date
        if last_active_delta < active_days_delta:
            active_users += 1

    click.echo(f"Active users: { active_users }")


def gl_api(host, token):
    """Authenticate to the GitLab API."""
    server_url = "https://{}".format(host)
    gl = gitlab.Gitlab(server_url, private_token=token)
    gl.auth()
    return gl


if __name__ == "__main__":
    cli()
